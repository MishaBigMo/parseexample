//
//  UsersListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    var users: [User] = []
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users = DataBaseManager.instance.getUsersFromDataBase()
//        ApiManager.instance.getUsers { (usersFromServer) in
//            for userFS in usersFromServer {
//                let user = User()
//                let company = Company()
//                let address = Address()
//                let geo = Geo()
//
//                user.name = userFS.name
//                user.id = userFS.id
//                user.username = userFS.username
//                user.email = userFS.email
//                user.phone = userFS.phone
//                user.website = userFS.website
//
//                company.name = userFS.company!.name
//                company.bs = userFS.company!.bs
//                company.catchPhrase = userFS.company!.catchPhrase
//                user.company = company
//
//                address.city = userFS.address!.city
//                address.street = userFS.address!.street
//                address.suite = userFS.address!.suite
//                address.zipcode = userFS.address!.zipcode
//                user.address = address
//
//                geo.lat = userFS.address!.geo!.lat
//                geo.lng = userFS.address!.geo!.lng
//                user.address?.geo = geo
//
//
//                self.users.append(user)
//                self.tableView.reloadData()
//                DataBaseManager.instance.saveUsersToDataBase(user: usersFromServer)
//            }
//        }
    }
    

}

extension UsersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell6", for: indexPath) as! UsersCell
        let user = users[indexPath.row]
        cell.nameLabel.text = user.name
        cell.idLabel.text = String(user.id)
        cell.emailLabel.text = user.email
        cell.usernameLabel.text = user.username
        cell.phone.text = user.phone
        cell.website.text = user.website
        cell.companyName.text = user.company?.name
        cell.cathPhrase.text = user.company?.catchPhrase
        cell.bs.text = user.company?.bs
        cell.addressStreetLabel.text = user.address?.street
        cell.suiteLabel.text = user.address?.suite
        cell.cityLabel.text = user.address?.city
        cell.zipcode.text = user.address?.zipcode
        cell.geoLat.text = user.address?.geo?.lat
        cell.geoLng.text = user.address?.geo?.lng

        return cell
    }
    
    
}
