//
//  PhotosListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Kingfisher
import UIKit

class PhotosListViewController: UIViewController {
    // MARK: - Properties
    var photos: [Photo] = []
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Actions
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
       // photos = DataBaseManager.instance.getPhotosFromDataBase()
        ApiManager.instance.getPhotos { (photoFromServer) in
            for i in photoFromServer {
                let photo = Photo()
                photo.albumId = i.albumId
                photo.id = i.id
                photo.title = i.title
                photo.url = i.url
                photo.thumbnailUrl = i.thumbnailUrl
                self.photos.append(photo)
                self.tableView.reloadData()
            }
            DataBaseManager.instance.savePhotosToDataBase(photos: photoFromServer)
        }
    }


}


extension PhotosListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell4", for: indexPath) as! PhCell
        let photo = photos[indexPath.row]
        let bigUrl = URL(string: "\(photo.url)")
        let littleUrl = URL(string: "\(photo.thumbnailUrl)")
        cell.label12.text = "\(photo.albumId)-\(photo.id)"
        cell.label3.text = photo.title
        cell.imageBig.kf.setImage(with: bigUrl)
        cell.imageLittle.kf.setImage(with: littleUrl)
        return cell
    }
    
    
    
}
