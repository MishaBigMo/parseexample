//
//  AlbumsListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class AlbumsListViewController: UIViewController {
    // MARK: - Properties
    var albums: [Album] = []
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Actions

    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        albums = DataBaseManager.instance.getAlbumsFromDataBase()
//        ApiManager.instance.getAlbums { (albumsFromServer) in
//            for i in albumsFromServer {
//                let album = Album()
//                album.id = i.id
//                album.userId = i.userId
//                album.title = i.title
//                self.albums.append(album)
//                self.tableView.reloadData()
//                DataBaseManager.instance.saveAlbumsToDataBase(albums: albumsFromServer)
//            }
//        }
    }
    
    
}

extension AlbumsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath) as! AlbumCell
        let album = albums[indexPath.row]
        cell.label1.text = String(album.id)
        cell.label2.text = String(album.userId)
        cell.label3.text = album.title
        
        return cell
    }
    
    
}
