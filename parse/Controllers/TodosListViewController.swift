//
//  TodosListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class TodosListViewController: UIViewController {
    // MARK: - Properties
    var todoses: [Todos] = []
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Actions
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        todoses = DataBaseManager.instance.getTodosFromDataBase()
//        ApiManager.instance.getTodos { (todosFromServer) in
//            for i in todosFromServer {
//                let todos = Todos()
//                todos.id = i.id
//                todos.userId = i.userId
//                todos.title = i.title
//                todos.completed = i.completed
//                self.todoses.append(todos)
//                self.tableView.reloadData()
//                DataBaseManager.instance.saveTodosToDataBase(todos: todosFromServer)
//            }
//        }
    }
    


}


extension TodosListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell5", for: indexPath) as! TodosCell
        let todos = todoses[indexPath.row]
        cell.label12.text = "\(todos.userId)-\(todos.id)"
        cell.label3.text = "\(todos.title)"
        cell.label4.text = "\(todos.completed)"
        return cell
    }
    
    
    
}
