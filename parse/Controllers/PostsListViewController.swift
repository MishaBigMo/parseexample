//
//  PostsListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class PostsListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var posts: [Post] = []
    
    
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        posts = DataBaseManager.instance.getPostFromDatabase()
        tableView.delegate = self
        tableView.dataSource = self
//        ApiManager.instance.getPosts { (postsFromServer) in
//            for post in postsFromServer {
//                let postFS = Post()
//                postFS.id = post.id
//                postFS.userId = post.userId
//                postFS.title = post.title
//                postFS.body = post.body
//                self.posts.append(postFS)
//                self.tableView.reloadData()
//                DataBaseManager.instance.savePostToDataBase(posts: postsFromServer)
//            }
//
//        }
    }
}

extension PostsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! PostCell
        let post = posts[indexPath.row]

        cell.label1.text = String(post.id)
        cell.label2.text = post.body
        cell.label4.text = post.title
        return cell
    }
    
    
}
