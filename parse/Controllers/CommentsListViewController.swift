//
//  CommentsListViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class CommentsListViewController: UIViewController {
    
    var comments: [Comment] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        comments = DataBaseManager.instance.getCommentsFromDataBase()
//        ApiManager.instance.getComments { (commentsFromServer) in
//            for i in commentsFromServer {
//                let comment = Comment()
//                comment.postId = i.postId
//                comment.id = i.id
//                comment.name = i.name
//                comment.email = i.email
//                comment.body = i.body
//                self.comments.append(comment)
//                self.tableView.reloadData()
//                DataBaseManager.instance.saveCommentsToDataBase(comments: commentsFromServer)
//            }
//
//        }
    }
    
}


extension CommentsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! CommentsCell
        let comment = comments[indexPath.row]
        
        cell.label1.text = String(comment.postId)
        cell.label2.text = String(comment.id)
        cell.label3.text = comment.name
        cell.label4.text = comment.email
        cell.label5.text = comment.body
        return cell
    }
    
    
}
