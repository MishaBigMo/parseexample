//
//  UIViewController+CoreData.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit
import Foundation
extension UIViewController {
    static func getFromStoryboard (withId id: String) -> UIViewController? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
}
