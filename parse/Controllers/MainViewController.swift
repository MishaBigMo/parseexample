//
//  ViewController.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    // MARK: - Properties

    // MARK: - Outlets
    
    // MARK: - Actions
    @IBAction func postsDidTapped(_ sender: Any) {
        let postsVC = UIViewController.getFromStoryboard(withId: "PostsVC")
        navigationController?.pushViewController(postsVC!, animated: true)
    }
    @IBAction func commentsDidTapped(_ sender: Any) {
        let commentsVC = UIViewController.getFromStoryboard(withId: "CommentsVC")
        navigationController?.pushViewController(commentsVC!, animated: true)
    }
    @IBAction func albumsDidTapped(_ sender: Any) {
        let albumsVC = UIViewController.getFromStoryboard(withId: "AlbumsVC")
        navigationController?.pushViewController(albumsVC!, animated: true)
    }
    @IBAction func photosDidTapped(_ sender: Any) {
        let photosVC = UIViewController.getFromStoryboard(withId: "PhotosVC")
        navigationController?.pushViewController(photosVC!, animated: true)
    }
    @IBAction func todosDidTapped(_ sender: Any) {
        let todosVC = UIViewController.getFromStoryboard(withId: "TodosVC")
        navigationController?.pushViewController(todosVC!, animated: true)
    }
    @IBAction func usersDidTapped(_ sender: Any) {
        let usersVC = UIViewController.getFromStoryboard(withId: "UsersVC")
        navigationController?.pushViewController(usersVC!, animated: true)
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }


}

