//
//  CommentsCell.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import UIKit

import Foundation

class CommentsCell: UITableViewCell {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
}
