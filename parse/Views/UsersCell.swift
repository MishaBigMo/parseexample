//
//  UsersCell.swift
//  parse
//
//  Created by Михаил on 07/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class UsersCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressStreetLabel: UILabel!
    @IBOutlet weak var suiteLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var zipcode: UILabel!
    @IBOutlet weak var geoLat: UILabel!
    @IBOutlet weak var geoLng: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var cathPhrase: UILabel!
    @IBOutlet weak var bs: UILabel!
    
  

}
