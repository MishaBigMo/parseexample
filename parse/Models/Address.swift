//
//  Address.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation
class Address {
    var street: String = ""
    var suite: String = ""
    var city: String = ""
    var zipcode: String = ""
    var geo: Geo?
    
//    enum CodingKeys : String, CodingKey {
//        case street
//        case suite
//        case city
//        case zipcode
//        case geo
//    }
//    required init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        
//        self.street = try! values.decode(String.self, forKey: .street)
//        self.suite = try! values.decode(String.self, forKey: .suite)
//        self.city = try! values.decode(String.self, forKey: .city)
//        self.zipcode = try! values.decode(String.self, forKey: .zipcode)
//        self.geo = try values.decode(Geo.self, forKey: .geo)
//        
//    }
}
