//
//  Todos.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation

class Todos {
    
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var completed: Bool = false
    
}

class TodosRealm: Object {
    
    @objc dynamic var userId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var completed: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
