//
//  ApiManager.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Alamofire
import RealmSwift
import Foundation

class ApiManager {
    // pattern - singleTorn
    static var instance = ApiManager()
    private enum Constants {
        static let baseURL = "https://jsonplaceholder.typicode.com"
    }
    
    private enum EndPoints {
        static let users = "/Users"
        static let todos = "/todos"
        static let photos = "/photos"
        static let albums = "/albums"
        static let comments = "/comments"
        static let posts = "/posts"
    }
    func getUsers(onComlete: @escaping ([User]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.users
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            //print(respons)
            
            switch respons.result {
            case .success(let data):
                //print(data)
                
                if let arrayUsers = data as? Array<Dictionary<String,Any>> {
                    var users: [User] = []
                    for userDict in arrayUsers {
                        let user = User()
                        user.id = userDict["id"] as? Int ?? 0 // Или if let
                        user.name = userDict["name"] as? String ?? "no data"
                        user.username = userDict["username"] as? String ?? "no data"
                        user.email = userDict["email"] as? String ?? "no data"
                        user.phone = userDict["phone"] as? String ?? "no data"
                        user.website = userDict["website"] as? String ?? "no data"
                        users.append(user)
                        
                        if let addressDict = userDict["address"] as? Dictionary<String,Any> {
                            let address = Address()
                            address.street = addressDict["street"] as? String ?? ""
                            address.suite = addressDict["suite"] as? String ?? ""
                            address.city = addressDict["city"] as? String ?? ""
                            address.zipcode = addressDict["zipcode"] as? String ?? ""
                            user.address = address
                            
                            if let geoDict = addressDict["geo"] as? Dictionary<String, Any> {
                                let geo = Geo()
                                geo.lat = geoDict["lat"] as? String ?? "no data"
                                geo.lng = geoDict["lng"] as? String ?? "no data"
                                user.address?.geo = geo
                                
                                if let companyDict = userDict["company"] as? Dictionary<String,Any> {
                                    let company = Company()
                                    company.name = companyDict["name"] as? String ?? "no data"
                                    company.catchPhrase = companyDict["catchPhrase"] as? String ?? "no data"
                                    company.bs = companyDict["bs"] as? String ?? "no data"
                                    user.company = company
                                }
                            }
                        }
                    }
                    onComlete(users)
                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getAlbums(onComlete: @escaping ([Album]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.albums
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            
            switch respons.result {
            case .success(let data):
                if let arrayAlbums = data as? Array<Dictionary<String,Any>> {
                    var albums: [Album] = []
                    for albumDict in arrayAlbums {
                        let album = Album()
                        album.userId = albumDict["userId"] as? Int ?? 0
                        album.id = albumDict["id"] as? Int ?? 0
                        album.title = albumDict["title"] as? String ?? "no data"
                        albums.append(album)
                    }
                    onComlete(albums)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPosts(onComlete: @escaping ([Post]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.posts
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            
            switch respons.result {
                
            case .success(let data):
                if let arrayPosts = data as? Array<Dictionary<String,Any>> {
                    var posts: [Post] = []
                    for postsDict in arrayPosts {
                        let post = Post()
                        post.userId = postsDict["userId"] as? Int ?? 0
                        post.id = postsDict["id"] as? Int ?? 0
                        post.title = postsDict["title"] as? String ?? "no data"
                        post.body = postsDict["body"] as? String ?? "no data"
                        posts.append(post)
                    }
                    onComlete(posts)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    func getComments(onComlete: @escaping ([Comment]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.comments
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            
            switch respons.result {
            case .success(let data):
                if let arrayComments = data as? Array<Dictionary<String,Any>> {
                    var comments: [Comment] = []
                    for commentsDict in arrayComments {
                        let comment = Comment()
                        comment.postId = commentsDict["postId"] as? Int ?? 0
                        comment.id = commentsDict["id"] as? Int ?? 0
                        comment.name = commentsDict["name"] as? String ?? "no data"
                        comment.email = commentsDict["email"] as? String ?? "no data"
                        comment.body = commentsDict["body"] as? String ?? "no data"
                        comments.append(comment)
                    }
                    onComlete(comments)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getPhotos(onComlete: @escaping ([Photo]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.photos
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            
            switch respons.result {
                
            case .success(let data):
                if let arrayPhotos = data as? Array<Dictionary<String,Any>> {
                    var photos: [Photo] = []
                    for photoDict in arrayPhotos {
                        let photo = Photo()
                        photo.albumId = photoDict["albumId"] as? Int ?? 0
                        photo.id = photoDict["id"] as? Int ?? 0
                        photo.title = photoDict["title"] as? String ?? "no data"
                        photo.url = photoDict["url"] as? String ?? "no data"
                        photo.thumbnailUrl = photoDict["thumbnailUrl"] as? String ?? "no data"
                        photos.append(photo)
                    }
                    onComlete(photos)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getTodos(onComlete: @escaping ([Todos]) -> Void) {
        let urlString =  Constants.baseURL + EndPoints.todos
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            //print(respons)
            
            switch respons.result {
            case .success(let data):
                //print(data)
                if let arrayTodos = data as? Array<Dictionary<String,Any>> {
                    var todoses: [Todos] = []
                    for todosDict in arrayTodos {
                        let todos = Todos()
                        
                        todos.userId = todosDict["userId"] as? Int ?? 0
                        todos.id = todosDict["id"] as? Int ?? 0
                        todos.title = todosDict["title"] as? String ?? "no data"
                        todos.completed = todosDict["completed"] as? Bool ?? false
                        todoses.append(todos)
                        
                    }
                    onComlete(todoses)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
