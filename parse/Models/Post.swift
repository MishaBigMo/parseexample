//
//  Post.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Foundation
import RealmSwift

class Post {
    
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var body: String = ""
}

class PostRealm: Object {
    
    @objc dynamic var userId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
