//
//  RealmPost.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation

class RealmPost {
    
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var body: String = ""
    
    
}
