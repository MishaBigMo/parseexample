//
//  Albums.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation

class Album {
    
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    
}

class AlbumRealm: Object {
    
    @objc dynamic var userId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
