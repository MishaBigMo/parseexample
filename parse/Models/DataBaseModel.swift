//
//  DataBasemodel.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation

class DataBaseManager {
    static var instance = DataBaseManager()
    
    func saveUsersToDataBase(user: [User]) {
        let realm = try! Realm()
        var usersRealm: [UserRealm] = []
        for userFS in user {
            let user = UserRealm()
            user.name = userFS.name
            user.id = userFS.id
            user.email = userFS.email
            user.username = userFS.username
            user.phone = userFS.phone
            user.website = userFS.website
            user.city = userFS.address?.city
            user.suite = userFS.address?.suite
            user.street = userFS.address?.street
            user.zipcode = userFS.address?.zipcode
            user.nameCompany = userFS.company?.name
            user.catchPhrase = userFS.company?.catchPhrase
            user.bs = userFS.company?.bs
            user.lat = userFS.address?.geo?.lat
            user.lng = userFS.address?.geo?.lng
            usersRealm.append(user)
        }
        try! realm.write {
            realm.add(usersRealm)
        }
    }
    
    func saveTodosToDataBase(todos: [Todos]) {
        let realm = try! Realm()
        var todosRealm: [TodosRealm] = []
        for todosFS in todos {
            let todos = TodosRealm()
            todos.id = todosFS.id
            todos.title = todosFS.title
            todos.completed = todosFS.completed
            todos.userId = todosFS.userId
            todosRealm.append(todos)
        }
        try! realm.write {
            realm.add(todosRealm, update: true)
        }
    }
    
    func savePhotosToDataBase(photos: [Photo]) {
        print(">>>>> savePhotosToDataBase")
        let realm = try! Realm()
        var photosRealm: [PhotoRealm] = []
        for photosFS in photos {
            let photo = PhotoRealm()
            photo.id = photosFS.id
            photo.title = photosFS.title
            photo.albumId = photosFS.albumId
            photo.thumbnailUrl = photosFS.thumbnailUrl
            photo.url = photosFS.url
            photosRealm.append(photo)
        }
        try! realm.write {
            realm.add(photosRealm, update: .all)
        }
    }
    
    func saveAlbumsToDataBase(albums: [Album]) {
        let realm = try! Realm()
        var albumsRealm: [AlbumRealm] = []
        for albumsFS in albums {
            let album = AlbumRealm()
            album.id = albumsFS.id
            album.title = albumsFS.title
            album.userId = albumsFS.userId
            albumsRealm.append(album)
        }
        try! realm.write {
            realm.add(albumsRealm, update: true)
        }
    }
    
    func saveCommentsToDataBase(comments: [Comment]) {
        let realm = try! Realm()
        var commentsRealm: [CommentsRealm] = []
        for commentsFS in comments {
            let comment = CommentsRealm()
            comment.name = commentsFS.name
            comment.id = commentsFS.id
            comment.body = commentsFS.body
            comment.email = commentsFS.email
            comment.postId = commentsFS.postId
            commentsRealm.append(comment)
        }
        try! realm.write {
            realm.add(commentsRealm, update: true)
        }
    }
    
    func savePostToDataBase (posts: [Post]) {
        let realm = try! Realm()
        var postRealm: [PostRealm] = []
        for postFS in posts {
            let post = PostRealm()
            post.id = postFS.id
            post.title = postFS.title
            post.userId = postFS.userId
            post.body = postFS.body
            postRealm.append(post)
        }
        try! realm.write {
            realm.add(postRealm, update: true)
        }
    }
    
    func getUsersFromDataBase() -> [User] {
        let realm = try! Realm()
        let usersResult = realm.objects(UserRealm.self)
        var users: [User] = []
        for realmUser in usersResult {
            let user = User()
            var company = Company()
            var address = Address()
            var geo = Geo()
            
            user.id = realmUser.id
            user.name = realmUser.name!
            user.email = realmUser.email!
            user.username = realmUser.username!
            user.phone = realmUser.phone!
            user.website = realmUser.website!
            company.name = realmUser.nameCompany!
            company.catchPhrase = realmUser.catchPhrase!
            company.bs = realmUser.bs!
            user.company = company
            address.street = realmUser.street!
            address.suite = realmUser.suite!
            address.city = realmUser.city!
            address.zipcode = realmUser.zipcode!
            user.address = address
            geo.lat = realmUser.lat!
            geo.lng = realmUser.lng!
            user.address?.geo = geo
//            user.address?.geo?.lat = realmUser.lat!
//            user.address?.geo?.lng = realmUser.lng!
            users.append(user)
        }
        return users
    }
    
    func getTodosFromDataBase() -> [Todos] {
        let realm = try! Realm()
        let todosResult = realm.objects(TodosRealm.self)
        var todoses: [Todos] = []
        for realmTodos in todosResult {
            let todos = Todos()
            todos.id = realmTodos.id
            todos.completed = realmTodos.completed
            todos.title = realmTodos.title
            todos.userId = realmTodos.userId
            todoses.append(todos)
        }
        return todoses
    }
    
    func getPhotosFromDataBase() -> [Photo] {
        let realm = try! Realm()
        let photoResult = realm.objects(PhotoRealm.self)
        var photos: [Photo] = []
        for realmPhoto in photoResult {
            let photo = Photo()
            photo.id = realmPhoto.id
            photo.albumId = realmPhoto.albumId
            photo.title = realmPhoto.title
            photo.url = realmPhoto.url
            photo.thumbnailUrl = realmPhoto.thumbnailUrl
            photos.append(photo)
        }
        return photos
    }
    
    func getAlbumsFromDataBase() -> [Album] {
        let realm = try! Realm()
        let albumResult = realm.objects(AlbumRealm.self)
        var albums: [Album] = []
        for realmAlbum in albumResult {
            let album = Album()
            album.id = realmAlbum.id
            album.userId = realmAlbum.userId
            album.title = realmAlbum.title
            albums.append(album)
        }
        return albums
    }
    
    func getCommentsFromDataBase() -> [Comment] {
        let realm = try! Realm()
        let commentsResult = realm.objects(CommentsRealm.self)
        var comments: [Comment] = []
        for realmComments in commentsResult {
            let comment = Comment()
            comment.body = realmComments.body
            comment.email = realmComments.email
            comment.id = realmComments.id
            comment.name = realmComments.name
            comment.postId = realmComments.postId
            comments.append(comment)
        }
        return comments
    }
    
    func getPostFromDatabase() -> [Post] {
        let realm = try! Realm()
        let postsResult = realm.objects(PostRealm.self)
        var posts: [Post] = []
        for realmPost in postsResult {
            let post = Post()
            post.id = realmPost.id
            post.body = realmPost.body
            post.title = realmPost.title
            post.userId = realmPost.userId
            posts.append(post)
        }
        return posts
    }
    


}
