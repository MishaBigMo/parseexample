//
//  Users.swift
//  parse
//
//  Created by Михаил on 06/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation
class User {
    
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    var address: Address?
    var company: Company?
    
//    enum CodingKeys : String, CodingKey {
//        case id
//        case name
//        case username
//        case email
//        case phone
//        case website
//        case address
//        case company
//    }
//    required init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//
//        self.id = try! values.decode(Int.self, forKey: .id)
//        self.name = try! values.decode(String.self, forKey: .name)
//        self.username = try! values.decode(String.self, forKey: .username)
//        self.email = try! values.decode(String.self, forKey: .email)
//        self.phone = try! values.decode(String.self, forKey: .phone)
//        self.website = try! values.decode(String.self, forKey: .website)
//        self.company = try! values.decode(Company.self, forKey: .company)
//        self.address = try values.decode(Address.self, forKey: .address)
//
//    }
}




class UserRealm: Object {
    
    @objc dynamic var id:  Int = 0
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var username: String?
    @objc dynamic var phone: String?
    @objc dynamic var website: String?
    @objc dynamic var suite: String?
    @objc dynamic var city: String?
    @objc dynamic var street: String?
    @objc dynamic var zipcode: String?
    @objc dynamic var nameCompany: String?
    @objc dynamic var catchPhrase: String?
    @objc dynamic var bs: String?
    @objc dynamic var lat: String?
    @objc dynamic var lng: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}


extension User: CustomStringConvertible {
    var description : String {
        return """
        \n
        id: \(id )
        name: \(name )
        username: \(username)
        email: \(email)
        phone:\(phone )
        website: \(website)
        \n
        company:
        \(company?.name)
        \(company?.bs))
        \(company?.catchPhrase))
        \n
        address:
        \(address?.city)
        \(address?.street)
        \(address?.suite)
        \(address?.zipcode)
        Geo:
        \(address?.geo?.lat)
        \(address?.geo?.lng)
        \n
        """
    }
}
